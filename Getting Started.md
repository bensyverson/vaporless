# ❄️ Getting Started with Vaporless

Getting Vaporless up and running with your project involves a few steps.

## Set up Serverless & OpenWhisk

First sign up for [IBM Cloud](https://cloud.ibm.com/), then follow the [Serverless](https://serverless.com/) framework [OpenWhisk Quick Start](https://serverless.com/framework/docs/providers/openwhisk/guide/quick-start/)

Here's a quick cheat sheet as of early 2019:

```
$ npm install --global serverless serverless-openwhisk
$ curl -sL https://ibm.biz/idt-installer | bash
$ ibmcloud login
$ ibmcloud target --cf
$ sls deploy -v

```

## Install Docker image

Your project gets cross-compiled using a Docker image that matches the OpenWhisk environment. We're currently using [`openwhisk/action-swift-v4.2`](https://hub.docker.com/r/openwhisk/action-swift-v4.2).

```
$ docker pull openwhisk/action-swift-v4.2
```

## Export your Vapor project as a library

If you don't have a Vapor project yet, you'll need to [create one](https://docs.vapor.codes/), e.g.:

 `$ vapor new MyProject`.

Next, add a library product to your Vapor app in your Vapor project's `Package.swift`. For example, something like this:

```swift
let package = Package(
    name: "myproject",
    products: [                                         // 👈 Library
        .library(name: "MyProject", targets: ["App"]),  // 👈 Added
    ],                                                  // 👈 Here
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
    ],
    targets: [
        .target(name: "App", dependencies: ["Vapor"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)
```

## Publish & Tag your Vapor project
Publish your project to Github or another Git hosting service, and **tag a release with a version number** such as `1.0.0` You will use this number when you import your Vapor app as a dependency.

## Create a new project

Next you'll need to create a new project which imports both Vaporless (this repo) and your own Vapor app.

The easiest way to do this is with a template. For example, to set up a [Vaporless project which uses OpenWhisk](https://gitlab.com/bensyverson/vaporless-openwhisk), try:

```
$ serverless create \
--template-url https://gitlab.com/bensyverson/vaporless-openwhisk \
--path MyApp
```

## Update the dependency to your Vapor project

In the newly generated Package.swift, update the placeholder dependency to point to your repo, with the version number of your tag:

```swift
.package(url: "https://github.com/me/vapor-app", from: "1.0.0")
```

If you're not using a template, be sure to add Vaporless as a dependency:

```swift
.package(url: "https://gitlab.com/bensyverson/vaporless", from: "0.1.5")
```

## Ensure that the new project is building natively

Make sure there are no compile errors, because the next step can be lengthy!

```
$ swift build
```

## Build the project for OpenWhisk and deploy it for the first time

The [`serverless.yml`](serverless.yml) file is configured to cross-compile the Swift source into a Zip when you run `serverless deploy`:

```
$ serverless deploy
```

This may take several minutes, depending on your computer.

## Development

### Incremental updates

As you work, you can deploy incremental updates rather than the entire service using `npm run update`:

```
$ npm run build
$ npm run update
```

`npm run update` is a shortcut for `serverless deploy --function main`

Note that deploying the function will not rebuild the Swift source, so you need to run `npm run build` before deploying the update.
