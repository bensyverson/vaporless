// swift-tools-version:4.2
import PackageDescription

let package = Package(
    name: "Vaporless",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "Vaporless",
            targets: ["Vaporless"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
		.package(url: "https://github.com/vapor/fluent-sqlite.git", from: "3.0.0")
   ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "Vaporless",
            dependencies: ["Vapor"]),
        .testTarget(
            name: "VaporlessTests",
            dependencies: ["Vaporless", "FluentSQLite"]),
    ]
)
