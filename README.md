# ❄️ Vaporless

The incredible ecosystem of [Vapor](https://vapor.codes/), wrapped up for [Serverless](https://serverless.com) deployment. 👏


## What even is this

 > 💡 Hey, would it be possible to use Vapor without all of the insanely fast [SwiftNIO](https://github.com/apple/swift-nio)-powered performance?
 
You, my friend, are in luck! More seriously, [FaaS](https://en.wikipedia.org/wiki/Function_as_a_service) (serverless) is sometimes the way to go. Maybe you just need to stand up one simple endpoint, maybe a client is demanding it, or maybe you're just prototyping.

But if you're a Vapor developer, it doesn't take long before you start itching for [Fluent](https://github.com/vapor/fluent), [Validation](https://github.com/vapor/validation), Vapor's awesome [use of Codable](https://docs.vapor.codes/3.0/getting-started/content/), and all of the tools built by the [community](https://github.com/vapor-community).

Vaporless lets you work on a classic monolithic Vapor app, and deploy the whole shebang as a single function—[Router](https://docs.vapor.codes/3.0/routing/getting-started/) and [Middleware](https://docs.vapor.codes/3.0/auth/api/#middleware) included! Currently only OpenWhisk is supported, but it should be possible to support AWS and other platforms.

## Quick Start

Vaporless is intended to be imported as a dependency along with your Vapor app in a new Swift project. For more information, see the [Getting Started](Getting Started.md) document, or check out the [Vaporless OpenWhisk Template](https://gitlab.com/bensyverson/vaporless-openwhisk).

To get up and running quickly, create a new project from the OpenWhisk template…

```
$ serverless create \
--template-url https://gitlab.com/bensyverson/vaporless-openwhisk \
--path MyApp
```

…then follow the instructions in the template's README.

## FAQ

> Q: Hey, is this going to allow my Vapor app to scale to infinity?

A: No

> Q: Help, my response times are 500ms or worse

A: Yeah

> Q: Any pro tips?

A: Turn off Fluent's autoMigrate property (and migrate manually) in your `configure.swift` to reduce the cold start delay:

```swift
try services.register(FluentProvider(autoMigrate: false))
```


## Other Questions?

Feel free to file Issues, PRs, etc! This is very much a work in progress.