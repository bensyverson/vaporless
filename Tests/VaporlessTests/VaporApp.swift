//
//  VaporApp.swift
//  Async
//
//  Created by Ben Syverson on 2019/03/06.
//

import XCTest
@testable import Vapor
@testable import FluentSQLite

/// A single entry of a Todo list.
struct TodoResponse: Codable, Equatable {
	var id: Int
	var title: String
}

final class Todo: SQLiteModel, Migration, Content, Parameter {
	var id: Int?
	var title: String
	init(id: Int? = nil, title: String) {
		self.id = id
		self.title = title
	}
}

final class TodoController {
	/// Returns a list of all `Todo`s.
	func index(_ req: Request) throws -> Future<[Todo]> {
		return Todo.query(on: req).all()
	}

	/// Gets a particular `Todo`
	func single(_ req: Request) throws -> Future<Todo> {
		return try req.parameters.next(Todo.self)
	}

	/// Saves a decoded `Todo` to the database.
	func create(_ req: Request) throws -> Future<Todo> {
		return try req.content.decode(Todo.self).flatMap { todo in
			return todo.save(on: req)
		}
	}

	/// Deletes a parameterized `Todo`.
	func delete(_ req: Request) throws -> Future<HTTPStatus> {
		return try req.parameters.next(Todo.self).flatMap { todo in
			return todo.delete(on: req)
			}.transform(to: .ok)
	}
}

struct VaporApp {
	/// Creates an instance of `Application` to use for testing
	public static func app(_ env: Environment) throws -> Application {
		var config = Config.default()
		var env = env
		var services = Services.default()
		try configure(&config, &env, &services)
		let app = try Application(config: config, environment: env, services: services)
		return app
	}

	/// Configure the routes for our dummy `Application`
	public static func routes(_ router: Router) throws {
		func data(_ req: Request) throws -> Response {
			guard let data = Data(base64Encoded: TestableValues.base64Data.rawValue) else {
				throw Abort(.internalServerError, reason: "Couldn't convert Base64 data")
			}
			return req.response(data, as: .binary)
		}
		router.get(TestableValues.hello.rawValue) { _ in return TestableValues.helloWorld.rawValue }
		router.get(TestableValues.bytes.rawValue, use: data)
		let todoController = TodoController()
		router.get(TestableValues.todos.rawValue, use: todoController.index)
		router.get(TestableValues.todos.rawValue, Todo.parameter, use: todoController.single)
		router.post(TestableValues.todos.rawValue, use: todoController.create)
		router.delete(TestableValues.todos.rawValue, Todo.parameter, use: todoController.delete)
	}

	/// Configure the `Application` instance
	public static func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
		try services.register(FluentSQLiteProvider())

		let router = EngineRouter.default()
		try routes(router)
		services.register(router, as: Router.self)

		var middlewares = MiddlewareConfig()
		middlewares.use(ErrorMiddleware.self)
		services.register(middlewares)

		let sqlite = try SQLiteDatabase(storage: .memory)
		var databases = DatabasesConfig()
		databases.add(database: sqlite, as: .sqlite)
		services.register(databases)

		var migrations = MigrationConfig()
		migrations.add(model: Todo.self, database: .sqlite)
		services.register(migrations)
	}
}
