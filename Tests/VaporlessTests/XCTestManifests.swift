import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(OpenWhiskRequestTests.allTests),
		testCase(OpenWhiskVaporRequestTests.allTests),
		testCase(OpenWhiskVaporlessTests.allTests),
    ]
}
#endif
