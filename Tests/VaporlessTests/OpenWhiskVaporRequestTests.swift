//
//  VaporRequestTests.swift
//  VaporlessTests
//
//  Created by Ben Syverson on 2019/03/06.
//

import XCTest
@testable import Vaporless
@testable import Vapor

final class OpenWhiskVaporRequestTests: XCTestCase {
	func vaporRequest() -> Request? {
		let args = postTodoWithTitleArgs()
		let inJson = try! JSONSerialization.data(withJSONObject: args, options: [])
		let req = try! OpenWhiskRequest(args: postTodoWithTitleArgs())
		return try? req.request(using: app, json: inJson)
	}
	
	func testVaporRequestIsNotNil() {
		XCTAssertNotNil(vaporRequest())
	}
	
	func testVaporRequestIsParsed() {
		let req = vaporRequest()!
		XCTAssertEqual(req.http.method, .POST)
		XCTAssertEqual(req.http.urlString, "/" + TestableValues.todos.rawValue)
		XCTAssertNotNil(req.http.headers[TestableValues.acceptEncoding.rawValue])
		XCTAssertEqual(req.http.headers[TestableValues.acceptEncoding.rawValue], [TestableValues.gzip.rawValue])
		
		let data = req.http.body.data
		XCTAssertNotNil(req.http.body.data)
		let obj = try? JSONSerialization.jsonObject(with: data!, options: [])
		XCTAssertNotNil(obj)
		let dict = obj as? [String: Any]
		XCTAssertNotNil(dict)
		let val = dict![TestableValues.title.rawValue]
		XCTAssertNotNil(val)
		let stringValue = val as? String
		XCTAssertNotNil(stringValue)
		XCTAssertEqual(stringValue!, TestableValues.updateReadme.rawValue)
	}

	func testVaporRequestConvenienceMethodReturnsValue() {
		let req = try? Request.from(postTodoWithTitleArgs(), with: app)
		XCTAssertNotNil(req)
	}
	
	func testVaporRequestConvenienceMethodParsed() {
		let req = try! Request.from(postTodoWithTitleArgs(), with: app)
		XCTAssertEqual(req.http.method, .POST)
		XCTAssertEqual(req.http.urlString, "/" + TestableValues.todos.rawValue)
		XCTAssertNotNil(req.http.headers[TestableValues.acceptEncoding.rawValue])
		XCTAssertEqual(req.http.headers[TestableValues.acceptEncoding.rawValue], [TestableValues.gzip.rawValue])
		
		let data = req.http.body.data
		XCTAssertNotNil(req.http.body.data)
		let obj = try? JSONSerialization.jsonObject(with: data!, options: [])
		XCTAssertNotNil(obj)
		let dict = obj as? [String: Any]
		XCTAssertNotNil(dict)
		let val = dict![TestableValues.title.rawValue]
		XCTAssertNotNil(val)
		let stringValue = val as? String
		XCTAssertNotNil(stringValue)
		XCTAssertEqual(stringValue!, TestableValues.updateReadme.rawValue)
	}

	static var allTests = [
		("testVaporRequestIsNotNil", testVaporRequestIsNotNil),
		("testVaporRequestIsParsed", testVaporRequestIsParsed),
		("testVaporRequestConvenienceMethodReturnsValue", testVaporRequestConvenienceMethodReturnsValue),
		("testVaporRequestConvenienceMethodParsed", testVaporRequestConvenienceMethodParsed),
	]
}
