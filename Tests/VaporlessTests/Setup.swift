//
//  Setup.swift
//  VaporlessTests
//
//  Created by Ben Syverson on 2019/03/06.
//

import Foundation

enum TestableValues: String {
	case hello, todos, bytes
	case get, post, delete
	case acceptEncoding = "accept-encoding"
	case gzip
	case helloWorld = "Hello, World!"
	case arrayJson = "[{\"title\":\"Item 1\"},{\"title\":\"Item 2\"}]"
	case objectJson = "{\"title\":\"Update readme\"}"
	case id, title
	case updateReadme = "Update readme"
	case body, statusCode, headers
	case contentLength = "content-length"
	case helloContentLength = "13"
	case postTodoContentLength = "32"
	case contentType = "content-type"
	case plainText = "text/plain; charset=utf-8"
	case applicationJson = "application/json; charset=utf-8"
	case base64Data = "QVNFRgABAAAAAAABAAEAAAAgAAYASABlAGwAbABvAABSR0IgAAAAAAAAAAA/gAAAAAE="
}

enum TestableIntegers: UInt {
	case success = 200
	case firstTodoId = 1
	case secondTodoId = 2
}

let app = try! VaporApp.app(.detect())
