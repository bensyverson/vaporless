//
//  OpenWhiskTestRequest.swift
//  Async
//
//  Created by Ben Syverson on 2019/03/06.
//

import Foundation
@testable import Vaporless

func owRequest(method: String, path: String, body: String? = nil) -> [String: Any] {
	var req: [String: Any] = [
		"__ow_headers": [
			"accept-encoding": "gzip",
			"cdn-loop": "cloudflare",
			"cf-connecting-ip": "127.0.0.1",
			"cf-ipcountry": "US",
			"cf-ray": "203948029384-ORD",
			"cf-visitor": "{\"scheme\":\"https\"}",
			"cookie": "__cfduid=293840293840923",
			"host": "us-south.functions.cloud.ibm.com",
			"user-agent": "Paw/3.1.8 (Macintosh; OS X/10.14.3) GCDHTTPRequest",
			"x-forwarded-for": "127.0.0.1, 127.0.0.1",
			"x-forwarded-host": "us-south.functions.cloud.ibm.com",
			"x-forwarded-port": "443",
			"x-forwarded-proto": "https",
			"x-global-k8fdic-transaction-id": "98273498273498",
			"x-real-ip": "127.0.0.1",
			"x-request-id": "908230948203948",
			"content-type": "application/json"
		],
		"__ow_method": method,
		"__ow_path": path
	]
	if let body = body {
		req["__ow_body"] = body
	}
	return req
}

func getHelloArgs() -> [String: Any] {
	 return owRequest(method: TestableValues.get.rawValue, path: "/" + TestableValues.hello.rawValue)
}

func getHello() -> OpenWhiskRequest? {
	return try? OpenWhiskRequest(args: getHelloArgs())
}

func postTodo(body: String? = nil) -> OpenWhiskRequest? {
	let args = owRequest(method: TestableValues.post.rawValue, path: "/" + TestableValues.todos.rawValue, body: body)
	return try? OpenWhiskRequest(args: args)
}

func postTodoWithTitleArgs() -> [String: Any] {
	var args = owRequest(method: TestableValues.post.rawValue, path: "/" + TestableValues.todos.rawValue)
	args[TestableValues.title.rawValue] = TestableValues.updateReadme.rawValue
	return args
}

func postTodoWithTitle() -> OpenWhiskRequest? {
	return try? OpenWhiskRequest(args: postTodoWithTitleArgs())
}

func getTodosArgs() -> [String: Any] {
	return owRequest(method: TestableValues.get.rawValue, path: "/" + TestableValues.todos.rawValue)
}

func getFirstTodoArgs() -> [String: Any] {
	return owRequest(method: TestableValues.get.rawValue,
					 path: "/" + TestableValues.todos.rawValue + "/\(TestableIntegers.firstTodoId.rawValue)")
}

func getBytesArgs() -> [String: Any] {
	return owRequest(method: TestableValues.get.rawValue, path: "/" + TestableValues.bytes.rawValue)
}

func deleteFirstTodoArgs() -> [String: Any] {
	return owRequest(method: TestableValues.delete.rawValue,
					 path: "/" + TestableValues.todos.rawValue + "/\(TestableIntegers.firstTodoId.rawValue)")
}
