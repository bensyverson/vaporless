//
//  VaporlessTests.swift
//  VaporlessTests
//
//  Created by Ben Syverson on 2019/03/06.
//

import XCTest
@testable import Vaporless
@testable import Vapor

final class OpenWhiskVaporlessTests: XCTestCase {
	let vaporless = Vaporless(app: app)

	func testVaporlessHelloWorldStatus() {
		let resp = vaporless.handle(args: getHelloArgs())
		let statusCode = resp[TestableValues.statusCode.rawValue]
		XCTAssertNotNil(statusCode)
		let code = statusCode as? UInt
		XCTAssertEqual(code, TestableIntegers.success.rawValue)
	}

	func testVaporlessHelloWorldBody() {
		let resp = vaporless.handle(args: getHelloArgs())
		let body = resp[TestableValues.body.rawValue]
		XCTAssertNotNil(body)
		let bodyString = body as? String
		XCTAssertEqual(bodyString, TestableValues.helloWorld.rawValue)
	}

	func testVaporlessHelloWorldHeaders() {
		let resp = vaporless.handle(args: getHelloArgs())

		let bodyString = resp[TestableValues.body.rawValue] as? String

		let headers = resp[TestableValues.headers.rawValue]
		XCTAssertNotNil(headers)
		let head = headers as? [String: String]
		XCTAssertNotNil(head)

		let contentLength = head![TestableValues.contentLength.rawValue]
		XCTAssertNotNil(contentLength)
		XCTAssertEqual(contentLength, TestableValues.helloContentLength.rawValue)

		/// Bit of a sanity check—let's make sure Vapor is returning the correct `content-length` header
		let uintLength = UInt(contentLength!)
		XCTAssertNotNil(uintLength)
		XCTAssertEqual(uintLength!, UInt(bodyString!.count))

		let contentType = head![TestableValues.contentType.rawValue]
		XCTAssertNotNil(contentType)
		XCTAssertEqual(contentType, TestableValues.plainText.rawValue)
	}

	func testVaporlessPostTodo1ReturnsTodo() {
		let resp = vaporless.handle(args: postTodoWithTitleArgs())

		let statusCode = resp[TestableValues.statusCode.rawValue]
		XCTAssertNotNil(statusCode)

		let code = statusCode as? UInt
		XCTAssertEqual(code, TestableIntegers.success.rawValue)

		let bodyString = resp[TestableValues.body.rawValue] as? String
		XCTAssertNotNil(bodyString)

		let bodyData = bodyString!.data(using: .utf8)
		XCTAssertNotNil(bodyData)

		let parsed = try? JSONSerialization.jsonObject(with: bodyData!, options: [])
		XCTAssertNotNil(parsed)

		let dict = parsed as? [String: Any]
		XCTAssertNotNil(dict)

		let id = dict![TestableValues.id.rawValue]
		XCTAssertNotNil(id)

		let idValue = id as? UInt
		XCTAssertEqual(idValue, TestableIntegers.firstTodoId.rawValue)

		let title = dict![TestableValues.title.rawValue]
		XCTAssertNotNil(title)

		let titleString = title as? String
		XCTAssertEqual(titleString, TestableValues.updateReadme.rawValue)

		let headers = resp[TestableValues.headers.rawValue]
		XCTAssertNotNil(headers)

		let head = headers as? [String: String]
		XCTAssertNotNil(head)

		let contentLength = head![TestableValues.contentLength.rawValue]
		XCTAssertNotNil(contentLength)
		XCTAssertEqual(contentLength, TestableValues.postTodoContentLength.rawValue)

		let contentType = head![TestableValues.contentType.rawValue]
		XCTAssertNotNil(contentType)
		XCTAssertEqual(contentType, TestableValues.applicationJson.rawValue)
	}

	func testVaporlessPostTodo2ReturnsCodable() {
		let resp = vaporless.handle(args: postTodoWithTitleArgs())

		let code = resp[TestableValues.statusCode.rawValue]! as? UInt
		XCTAssertEqual(code, TestableIntegers.success.rawValue)

		let bodyData = (resp[TestableValues.body.rawValue] as! String).data(using: .utf8)
		XCTAssertNotNil(bodyData)

		let expectedTodo = TodoResponse(id: Int(TestableIntegers.secondTodoId.rawValue),
										title: TestableValues.updateReadme.rawValue)
		let returnedTodo = try? JSONDecoder().decode(TodoResponse.self, from: bodyData!)
		XCTAssertNotNil(returnedTodo)
		XCTAssertEqual(returnedTodo!, expectedTodo)
	}

	func testVaporlessPostTodosGetFirst() {
		let resp = vaporless.handle(args: getFirstTodoArgs())

		let code = resp[TestableValues.statusCode.rawValue]! as? UInt
		XCTAssertEqual(code, TestableIntegers.success.rawValue)

		let bodyData = (resp[TestableValues.body.rawValue] as! String).data(using: .utf8)
		XCTAssertNotNil(bodyData)

		let expectedTodo = TodoResponse(id: Int(TestableIntegers.firstTodoId.rawValue),
										title: TestableValues.updateReadme.rawValue)
		let returnedTodo = try? JSONDecoder().decode(TodoResponse.self, from: bodyData!)
		XCTAssertNotNil(returnedTodo)
		XCTAssertEqual(returnedTodo!, expectedTodo)
	}

	func testVaporlessPostTodosReturned() {
		let resp = vaporless.handle(args: getTodosArgs())

		let code = resp[TestableValues.statusCode.rawValue]! as? UInt
		XCTAssertEqual(code, TestableIntegers.success.rawValue)

		let bodyData = (resp[TestableValues.body.rawValue] as! String).data(using: .utf8)
		XCTAssertNotNil(bodyData)

		let todos = [
			TodoResponse(id: Int(TestableIntegers.firstTodoId.rawValue),
						 title: TestableValues.updateReadme.rawValue),
			TodoResponse(id: Int(TestableIntegers.secondTodoId.rawValue),
						 title: TestableValues.updateReadme.rawValue)
		]
		let returnedTodos = try? JSONDecoder().decode([TodoResponse].self, from: bodyData!)
		XCTAssertNotNil(returnedTodos)
		XCTAssertEqual(returnedTodos!, todos)
	}

	func testVaporlessThenDeleteFirst() {
		let resp = vaporless.handle(args: deleteFirstTodoArgs())

		let code = resp[TestableValues.statusCode.rawValue]! as? UInt
		XCTAssertEqual(code, TestableIntegers.success.rawValue)
	}

	func testVaporlessGetData() {
		let resp = vaporless.handle(args: getBytesArgs())

		let code = resp[TestableValues.statusCode.rawValue]! as? UInt
		XCTAssertEqual(code, TestableIntegers.success.rawValue)

		let bodyString = resp[TestableValues.body.rawValue] as! String
		XCTAssertNotNil(bodyString)
		XCTAssertEqual(bodyString, TestableValues.base64Data.rawValue)
	}

	static var allTests = [
		("testVaporlessGetData", testVaporlessGetData),
		("testVaporlessHelloWorldBody", testVaporlessHelloWorldBody),
		("testVaporlessHelloWorldHeaders", testVaporlessHelloWorldHeaders),
		("testVaporlessHelloWorldStatus", testVaporlessHelloWorldStatus),
		("testVaporlessPostTodo1ReturnsTodo", testVaporlessPostTodo1ReturnsTodo),
		("testVaporlessPostTodo2ReturnsCodable", testVaporlessPostTodo2ReturnsCodable),
		("testVaporlessPostTodosGetFirst", testVaporlessPostTodosGetFirst),
		("testVaporlessPostTodosReturned", testVaporlessPostTodosReturned),
		("testVaporlessThenDeleteFirst", testVaporlessThenDeleteFirst),
	]
}
