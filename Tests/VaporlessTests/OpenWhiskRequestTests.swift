import XCTest
@testable import Vaporless

final class OpenWhiskRequestTests: XCTestCase {
    func testOpenWhiskGetRequestIsNotNil() {
		let req = getHello()
        XCTAssertNotNil(req)
    }
	
	func testOpenWhiskGetRequestIsParsed() {
		let req = getHello()!
		XCTAssertEqual(req.method, TestableValues.get.rawValue)
		XCTAssertEqual(req.path, "/" + TestableValues.hello.rawValue)
		XCTAssertNotNil(req.headers[TestableValues.acceptEncoding.rawValue])
		XCTAssertEqual(req.headers[TestableValues.acceptEncoding.rawValue], TestableValues.gzip.rawValue)
	}
	
	func testOpenWhiskPostRequestIsNotNil() {
		let req = postTodo(body: TestableValues.arrayJson.rawValue)
		XCTAssertNotNil(req)
	}
	
	func testOpenWhiskPostRequestIsParsed() {
		let req = postTodo(body: TestableValues.arrayJson.rawValue)!
		XCTAssertEqual(req.method, TestableValues.post.rawValue)
		XCTAssertEqual(req.path, "/" + TestableValues.todos.rawValue)
		XCTAssertNotNil(req.headers[TestableValues.acceptEncoding.rawValue])
		XCTAssertEqual(req.headers[TestableValues.acceptEncoding.rawValue], TestableValues.gzip.rawValue)
		XCTAssertEqual(req.body, TestableValues.arrayJson.rawValue)
	}

	func testOpenWhiskGetHTTPRequestIsNotNil() {
		let req = try? getHello()!.httpRequest()
		XCTAssertNotNil(req)
	}
	
	func testOpenWhiskPostHTTPRequestIsNotNil() {
		let req = try? postTodo(body: TestableValues.arrayJson.rawValue)!.httpRequest()
		XCTAssertNotNil(req)
	}
	
	func testOpenWhiskPostHTTPRequestIsParsed() {
		let req = try! postTodo(body: TestableValues.arrayJson.rawValue)!.httpRequest()
		let bodyData = TestableValues.arrayJson.rawValue.data(using: .utf8)!
		XCTAssertEqual(req.method, .POST)
		XCTAssertEqual(req.urlString, "/" + TestableValues.todos.rawValue)
		XCTAssertNotNil(req.headers[TestableValues.acceptEncoding.rawValue])
		XCTAssertEqual(req.headers[TestableValues.acceptEncoding.rawValue], [TestableValues.gzip.rawValue])
		XCTAssertEqual(req.body.data, bodyData)
	}

    static var allTests = [
		("testOpenWhiskGetRequestIsNotNil", testOpenWhiskGetRequestIsNotNil),
		("testOpenWhiskGetRequestIsParsed", testOpenWhiskGetRequestIsParsed),
		("testOpenWhiskPostRequestIsNotNil", testOpenWhiskPostRequestIsNotNil),
		("testOpenWhiskPostRequestIsParsed", testOpenWhiskPostRequestIsParsed),
		("testOpenWhiskGetHTTPRequestIsNotNil", testOpenWhiskGetHTTPRequestIsNotNil),
		("testOpenWhiskPostHTTPRequestIsNotNil", testOpenWhiskPostHTTPRequestIsNotNil),
		("testOpenWhiskPostHTTPRequestIsParsed", testOpenWhiskPostHTTPRequestIsParsed),
    ]
}
