//
//  OpenWhisk.swift
//  Action
//
//  Created by Ben Syverson on 2019/03/05.
//

import Foundation
import Vapor

struct OpenWhiskRequest: Codable {
	let headers: [String: String]
	let body: String?
	let method: String
	let path: String
	let query: String?
	
	enum OWHTTPMethod: String, Codable {
		case get, head, post, put, delete, connect, options, trace, patch
	}
	
	enum CodingKeys: String, CodingKey {
		case headers = "__ow_headers"
		case method = "__ow_method"
		case path = "__ow_path"
		case body = "__ow_body"
		case query = "__ow_query"
	}

	init(args: [String: Any]) throws {
		let inJson = try JSONSerialization.data(withJSONObject: args, options: [])
		self = try JSONDecoder().decode(OpenWhiskRequest.self, from: inJson)
	}
}

extension OpenWhiskRequest.OWHTTPMethod {
	var httpMethod: HTTPMethod {
		switch self {
		case .get:
			return .GET
		case .head:
			return .HEAD
		case .post:
			return .POST
		case .put:
			return .PUT
		case .delete:
			return .DELETE
		case .connect:
			return .CONNECT
		case .options:
			return .OPTIONS
		case .trace:
			return .TRACE
		case .patch:
			return .PATCH
		}
	}
}

extension OpenWhiskRequest {
	enum RequestError: Error {
		case badMethod(String)
	}
	func httpRequest() throws -> HTTPRequest {
		guard let verb = OWHTTPMethod(rawValue: method) else {
			throw RequestError.badMethod(method)
		}
		
		return HTTPRequest(
			method: verb.httpMethod,
			url: path,
			headers: HTTPHeaders(headers.map{ $0 }),
			body: body ?? HTTPBody()
		)
	}
}

struct OpenWhiskResponse: Codable {
	let body: String
	let statusCode: UInt
	let headers: [String: String]

	init(_ statusCode: HTTPStatus = .internalServerError,
			  message: String = "",
			  headers: [String: String] = ["Content-Type": "text/plain"]) {
		self.statusCode = statusCode.code
		self.body = message
		self.headers = headers
	}

	init(_ statusCode: HTTPStatus,
		 body: Data?,
		 headers: [String: String]) {
		self.statusCode = statusCode.code
		self.headers = headers
		guard let bodyData = body else {
			self.body = ""
			return
		}

		/// Prefer to encode the data as a UTF8 string.
		if let stringData = String(bytes: bodyData, encoding: .utf8) {
			self.body = stringData
		} else {
			/// Otherwise, encode it as base64
			self.body = bodyData.base64EncodedString()
		}
	}

	var bare: [String: Any] {
		return [
			"body": body,
			"statusCode": statusCode,
			"headers": headers
		]
	}
}
