import Foundation
import Vapor

extension OpenWhiskRequest {
	public func request(using app: Application, json: Data) throws -> Request {
		var httpRequest = try self.httpRequest()
		
		if body == nil {
			httpRequest.body = HTTPBody(data: json)
		}
		
		return Request(http: httpRequest, using: app)
	}
}

extension Request {
	public static func from(_ args: [String: Any], with app: Application) throws -> Request {
		let inJson = try JSONSerialization.data(withJSONObject: args, options: [])
		return try JSONDecoder()
			.decode(OpenWhiskRequest.self, from: inJson)
			.request(using: app, json: inJson)
	}
}

extension Vaporless {
	public func handle(args: [String: Any]) -> [String: Any] {
		do {
			let request = try Request.from(args, with: app )
			
			guard let router = try? app.make(Router.self),
				let responder = router.route(request: request) else {
				return OpenWhiskResponse(.notFound, message: "Not found: \(request.http.urlString)").bare
			}
			
			return try responder
				.respond(to: request)
				.encode(for: request)
				.map{ resp in
					OpenWhiskResponse(resp.http.status,
									  body: resp.http.body.data,
									  headers: resp.http.headers.reduce(into: [String: String]()){ $0[$1.name] = $1.value }).bare
				}.wait()
		} catch {
			print("Error in main body: \(error):\n\nArgs:\n\(args)")
			return OpenWhiskResponse(message: "Whoops! We encountered a problem.").bare
		}
	}
}
