//
//  Vaporless.swift
//  Action
//
//  Created by Ben Syverson on 2019/03/05.
//

import Foundation
import Vapor

public struct Vaporless {
	public let app: Application
	
	public init(app: Application) {
		self.app = app
	}
}
